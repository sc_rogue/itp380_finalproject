// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "InventoryWidget.generated.h"

/**
 * 
 */
UCLASS()
class ESCAPEPLAN_API UInventoryWidget : public UUserWidget
{
	GENERATED_BODY()

public:
    UFUNCTION(BlueprintImplementableEvent, Category = UI)
    void Show();
    
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
    TArray<APawn*> ItemsArray;
	
	
};
