// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "Door.h"
#include "DoorKey.generated.h"

UCLASS()
class ESCAPEPLAN_API ADoorKey : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ADoorKey();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
    
    void OpentheDoor();
    
    ADoor* GetDoor() { return thedoor; }
    
    float GetRange() { return Range; }
    
    FString GetName() { return Name; }
    
    FORCEINLINE UTexture2D* GetItemTexture() { return ItemTexture; }
    
protected:
    UPROPERTY(EditAnywhere)
    ADoor* thedoor;
    float Range = 150.0f;
    FString Name;
    
    UPROPERTY(EditAnywhere, Category = "ItemProperties")
    UTexture2D* ItemTexture;
	
};
