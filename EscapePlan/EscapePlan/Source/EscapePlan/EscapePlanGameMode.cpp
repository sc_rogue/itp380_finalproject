// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "EscapePlan.h"
#include "EscapePlanGameMode.h"
#include "EscapePlanPlayerController.h"
#include "EscapePlanCharacter.h"
#include "PlayerHUD.h"

AEscapePlanGameMode::AEscapePlanGameMode()
{
    
	// use our custom PlayerController class
	PlayerControllerClass = AEscapePlanPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
    //	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/MixamoAnimPack/Mixamo_Vanguard/Mixamo_Vanguard"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
    
    //set the default HUD class
    HUDClass = APlayerHUD::StaticClass();
    
}
