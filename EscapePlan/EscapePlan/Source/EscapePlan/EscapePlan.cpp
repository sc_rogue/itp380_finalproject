// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "EscapePlan.h"


IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, EscapePlan, "EscapePlan" );

DEFINE_LOG_CATEGORY(LogEscapePlan)
 