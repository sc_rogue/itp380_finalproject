// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapePlan.h"
#include "PlayerHUD.h"
#include "DoorKey.h"
#include "HealingPack.h"
#include "EscapePlanGameMode.h"
#include "EscapePlanCharacter.h"
#include "Engine/Canvas.h"
#include "Engine/Font.h"
#include "StaticNPC.h"

APlayerHUD::APlayerHUD()
{
    static ConstructorHelpers::FObjectFinder<UFont>HUDFontOb(TEXT("/Engine/EngineFonts/RobotoDistanceField"));
    HUDFont = HUDFontOb.Object;
}

void APlayerHUD::DrawHUD()
{
    FVector2D ScreenDimensions = FVector2D(Canvas->SizeX, Canvas->SizeY);
    Super::DrawHUD();

    AEscapePlanCharacter* MyCharacter = Cast<AEscapePlanCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
    FString HealthTitle = FString(TEXT("Health: "));
    FString HealthString = FString::SanitizeFloat(MyCharacter->GetHealth());
    HealthTitle += HealthString;
    DrawText(HealthTitle, FColor::Black, 10, 10, HUDFont);
    //FString ItemTitle = FString(TEXT("Item:"));
	FString Instruction = FString(TEXT("This is a dungeon crawling game, your goal is to find a way out.\nEach door has an corresponding key. You need to find that specific key to open each door.\nPress E to pick up the key. When you have the corrsponding key,\nstand close to the door and press E to open the door.\nYou can also press E to pick healing pack or weapon\nThe healing pack and key are stored in your bag, press tab to check your bag.\nPress space to jump and press enter to attack.\n"));
    //DrawText(ItemTitle, FColor::Red, 10, 30, HUDFont);
	for (FConstPawnIterator Iterator = GetWorld()->GetPawnIterator(); Iterator; ++Iterator)
	{
		APawn* Pawn = *Iterator;
		if (Pawn->IsA(AStaticNPC::StaticClass()))
		{
			if (Cast<AStaticNPC>(Pawn)->IsInRange())
			{
				DrawText(Instruction, FColor::Green, 300, 300, HUDFont);
			}
		}
	}
    TArray<APawn*> CharacterBackpack = MyCharacter->GetBackpack();
    
    /*for (int i = 0; i < CharacterBackpack.Num(); i++) {
        if(CharacterBackpack[i]==nullptr)return;
        if(CharacterBackpack[i]->IsA(ADoorKey::StaticClass()))
        {
            FString Name = Cast<ADoorKey>(CharacterBackpack[i])->GetName();
            DrawText(Name, FColor::Red, 10, Yposition, HUDFont);
            Yposition += 20;
        }
     
        if(CharacterBackpack[i]->IsA(AHealingPack::StaticClass()))
        {
            FString Name = Cast<AHealingPack>(CharacterBackpack[i])->GetName();
            DrawText(Name, FColor::Red, 10, Yposition, HUDFont);
            Yposition += 20;
        }
    }
    
    }*/
    Yposition = 50;
    if(MyCharacter->GetIsDead())
    {
        FVector2D GameOverSize;
        GetTextSize(TEXT("GAME OVER"), GameOverSize.X, GameOverSize.Y, HUDFont);
        DrawText(TEXT("GAME OVER"), FColor::White, (ScreenDimensions.X - GameOverSize.X) / 2.0f, (ScreenDimensions.Y - GameOverSize.Y) / 2.0f);
    }
    
    if(MyCharacter->GetIsWon())
    {
        FVector2D GameOverSize;
        GetTextSize(TEXT("YOU WIN"), GameOverSize.X, GameOverSize.Y, HUDFont);
        DrawText(TEXT("YOU WIN"), FColor::White, (ScreenDimensions.X - GameOverSize.X) / 2.0f, (ScreenDimensions.Y - GameOverSize.Y) / 2.0f);
    }
}


