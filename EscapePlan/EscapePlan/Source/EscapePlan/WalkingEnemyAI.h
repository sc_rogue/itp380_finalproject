// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "Engine/TargetPoint.h"
#include "WalkingEnemyAI.generated.h"

/**
 * 
 */
UCLASS()
class ESCAPEPLAN_API AWalkingEnemyAI : public AAIController
{
	GENERATED_BODY()
	
    void BeginPlay() override;
    void RegularWalking();
    virtual void Tick( float DeltaSeconds ) override;
    
    enum States{
        Start,
        Chase,
        Attack,
        Dead,
        Walk
    };
    void OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result)override;
protected:
    
    
private:
    void ChasePlayer();
    bool CheckPlayer();
    void StopChase();
    

    void AttackPlayer();
    States mState;

    int spotsnum;
    ATargetPoint* currentdestination;
    int currspot;
    float mRange;
    float attackRange;
    float checkRange;
    float distance;
    float sightRange;
    //speeds
    float RegularSpeed;
    float RunningSpeed;
    void SpeedUp();
    void SpeedDown();
    
    bool detected;

};
