// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapePlan.h"
#include "WalkingEnemyAI.h"
#include "EscapePlanCharacter.h"
#include "WalkingEnemy.h"

void AWalkingEnemyAI::BeginPlay(){
    Super::BeginPlay();
    
    APawn* pawn = UGameplayStatics::GetPlayerPawn(this, 0);
    MoveToActor(pawn);
    AWalkingEnemy* enemy = Cast<AWalkingEnemy>(GetPawn());
    spotsnum = enemy->SpotPoints.Num();
    currspot = -1;
    mRange = 150.0f;
    mState = Start;
    checkRange = 400.0f;
    sightRange = 5000.0f;
    attackRange = 150.0f;
    RunningSpeed = enemy->GetCharacterMovement()->GetMaxSpeed()-20.0f;
    RegularSpeed = 150.0f;
    detected=false;

    
}
void AWalkingEnemyAI::Tick( float DeltaTime )
{
    Super::Tick( DeltaTime );
    if(mState==Start||mState==Walk){
        SpeedDown();
        RegularWalking();

    }
    APawn* pawn = UGameplayStatics::GetPlayerPawn(this, 0);
    if(!pawn)return;
    distance =FMath::Abs(FVector::Dist(pawn->GetActorLocation(), GetPawn()->GetActorLocation()));
    
    
    if(mState==Chase){
     
        if(distance>checkRange&&!detected){
            StopChase();
            Cast<AWalkingEnemy>(GetPawn())->StopAttack();

        }
    }
    if(mState==Attack){

        if(distance<attackRange){
        }
        if(distance>attackRange&&distance<checkRange){
            ChasePlayer();
            mState = Chase;
            Cast<AWalkingEnemy>(GetPawn())->StopAttack();

        }
        if(distance>checkRange){
            StopChase();
            Cast<AWalkingEnemy>(GetPawn())->StopAttack();
        }
        
    }
    
}
void AWalkingEnemyAI::StopChase(){
    currspot = -1;
    RegularWalking();
    SpeedDown();
}
void AWalkingEnemyAI::SpeedUp(){
    AWalkingEnemy* enemy = Cast<AWalkingEnemy>(GetPawn());
    enemy->GetCharacterMovement()->MaxWalkSpeed = RunningSpeed;
    
}
void AWalkingEnemyAI::SpeedDown(){
    AWalkingEnemy* enemy = Cast<AWalkingEnemy>(GetPawn());
    enemy->GetCharacterMovement()->MaxWalkSpeed = RegularSpeed;
}
void AWalkingEnemyAI::AttackPlayer(){
    AWalkingEnemy* Walking = Cast<AWalkingEnemy>(GetPawn());
    Walking->StartAttack();
    
}
void AWalkingEnemyAI::ChasePlayer(){
    SpeedUp();
    APawn* pawn = UGameplayStatics::GetPlayerPawn(this, 0);
    MoveToActor(pawn);
}
bool AWalkingEnemyAI::CheckPlayer(){
    
    if(distance<attackRange){
        ChasePlayer();
        mState = Chase;
        return true;
    }
    
    
    FVector CurrLocation = GetPawn()->GetActorLocation();

    static FName SightTraceTag = FName(TEXT("SightTrace"));
    FVector Forward = GetPawn()->GetActorForwardVector();
    // Calculate end position
    FVector EndPos = CurrLocation + sightRange * Forward;
    // Perform trace to retrieve hit info
    FCollisionQueryParams TraceParams(SightTraceTag, true, Instigator);
    TraceParams.bTraceAsyncScene = true;
    TraceParams.bReturnPhysicalMaterial = true;
    // This fires the ray and checks against all objects w/ collision
    FHitResult Hit(ForceInit);
    GetWorld()->LineTraceSingleByObjectType(Hit,CurrLocation , EndPos,
                                            FCollisionObjectQueryParams::AllObjects, TraceParams);
    if (Hit.bBlockingHit)
    {
        AEscapePlanCharacter* Player = Cast<AEscapePlanCharacter>(Hit.GetActor());
        if (Player)
        {

          //s  UE_LOG(LogTemp, Warning, TEXT("adasdasdsa"));
            detected = true;
            ChasePlayer();
            mState = Chase;
        
            return true;
        }
    }
    return false;
}

void AWalkingEnemyAI::OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result){
    if(Result == EPathFollowingResult::Success){
        if(mState==Chase){
            detected = false;
            mState = Attack;
            AttackPlayer();
        }
        if(mState==Attack){
        //    AttackPlayer();
        }
    }
    
    
}

void AWalkingEnemyAI::RegularWalking(){
    if(CheckPlayer())return;
    if(spotsnum<1)return;
    if(currentdestination&&mState==Walk){
        if(FMath::Abs(FVector::Dist(GetPawn()->GetActorLocation(), currentdestination->GetActorLocation()))>mRange){
            return;
        }
    }
    mState = Walk;
    if(currspot==-1||spotsnum==currspot){
        currspot = 0;
    }
    
    currentdestination=Cast<AWalkingEnemy>(GetPawn())->SpotPoints[currspot];
    MoveToLocation(currentdestination->GetActorLocation());
    currspot++;


    
    

}
