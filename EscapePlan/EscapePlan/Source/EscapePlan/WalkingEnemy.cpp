// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapePlan.h"
#include "WalkingEnemy.h"
#include "EscapePlanCharacter.h"
#include "WalkingEnemyAI.h"

// Sets default values
AWalkingEnemy::AWalkingEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    AIControllerClass = AWalkingEnemyAI::StaticClass();


}

// Called when the game starts or when spawned
void AWalkingEnemy::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AWalkingEnemy::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}


// Called to bind functionality to input
void AWalkingEnemy::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

float AWalkingEnemy::TakeDamage(float Damage, FDamageEvent const& DamageEvent,
                                 AController* EventInstigator, AActor* DamageCauser)
{
    float ActualDamage = Super::TakeDamage(Damage, DamageEvent,
                                           EventInstigator, DamageCauser);
    if (ActualDamage > 0.0f)
    {
        Health -= ActualDamage;
        FVector Forward = GetActorForwardVector();
        FVector EndPos = GetActorLocation() - Forward * 100.0f;
      //  Cast<AAIController>(GetController())->MoveToLocation(EndPos);
        SetActorLocation(EndPos);

        if (Health <= 0.0f)
        {
            // We're dead, don't allow further damage
            bCanBeDamaged = false;
            float Duration = 0.1f;
            GetWorldTimerManager().SetTimer(DeathTimer, this, &AWalkingEnemy::EnemyDeath, Duration, false);
        }
    }
    return ActualDamage;
}

void AWalkingEnemy::StartAttack()
{
  //  UE_LOG(LogTemp, Warning, TEXT("attack"));
    float Duration = PlayAnimMontage(AttackAnim);
    GetWorldTimerManager().SetTimer(AttackTimer, this, &AWalkingEnemy::CauseDamage, Duration, true);
}

void AWalkingEnemy::StopAttack()
{
    //if (this != nullptr)
    StopAnimMontage(AttackAnim);
    GetWorldTimerManager().ClearTimer(AttackTimer);
}
void AWalkingEnemy::CauseDamage(){
    APawn * playerPawn = UGameplayStatics::GetPlayerPawn(this, 0);
    if (playerPawn != nullptr)
    {
        playerPawn->TakeDamage(Damage, FDamageEvent(), GetInstigatorController(), this);
    }
}

void AWalkingEnemy::EnemyDeath()
{
    printf("sdfsdfsdfsdf");
    Destroy();
    //SetIsDead(true);
}
