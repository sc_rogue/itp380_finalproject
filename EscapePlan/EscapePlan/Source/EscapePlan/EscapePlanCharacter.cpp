// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "EscapePlan.h"
#include "Engine.h"
#include "EscapePlanCharacter.h"
#include "WalkingEnemy.h"
#include "RifleWeapon.h"
#include "RotateEnemy.h"
#include "DoorKey.h"
#include "Door.h"
#include "HealingPack.h"
#include "EscapePlanGameMode.h"
#include "EngineUtils.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"

AEscapePlanCharacter::AEscapePlanCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->RelativeRotation = FRotator(-60.f, 0.f, 0.f);
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

										  // Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/TopDownCPP/Blueprints/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
    IsRunning = false;
    
    
    
    
   
}

void AEscapePlanCharacter::Tick(float DeltaSeconds)
{
	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params;
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
        
        if(GetVelocity().Size() != 0.0f)
        {
            IsRunning = true;
        }
        else
        {
            IsRunning = false;
        }
	}
}

void AEscapePlanCharacter::StartMeleeAttack()
{
    //trial
    TArray<USkeletalMeshComponent*> Components;
    this->GetComponents<USkeletalMeshComponent>(Components);
    for( int32 i=0; i<Components.Num(); i++ )
    {
        USkeletalMeshComponent* StaticMeshComponent = Components[i];
        USkeletalMesh* StaticMesh = StaticMeshComponent->SkeletalMesh;
        printf("%ls\n",*StaticMesh->GetName());
    }
    
    
    APawn* Pawn = UGameplayStatics::GetPlayerPawn(this, 0);
    if(Pawn != nullptr)
    {
        
        float Duration = PlayAnimMontage(AttackAnim);
        GetWorldTimerManager().SetTimer(AttackTimer, this, &AEscapePlanCharacter::Attack, Duration - 1.0f, false);
    }
    AGameMode *Mode = UGameplayStatics::GetGameMode(GetWorld());
}

void AEscapePlanCharacter::StopMeleeAttack()
{
    StopAnimMontage(AttackAnim);
    GetWorldTimerManager().ClearTimer(AttackTimer);
}

void AEscapePlanCharacter::StartJump()
{
    if(!IsRunning)
    {
        PlayAnimMontage(JumpAnim);
        GetWorldTimerManager().SetTimer(JumpTimer, this, &AEscapePlanCharacter::Jump, 0.718f, false);
    }
    else
    {
        PlayAnimMontage(RunningJumpAnim);
        Jump();
    }
}

void AEscapePlanCharacter::Attack()
{
    for (TActorIterator<AWalkingEnemy> ActorItr(GetWorld()); ActorItr; ++ActorItr)
    {
        if(ActorItr->IsA(AWalkingEnemy::StaticClass()))
        {
            FVector EmyLocation = ActorItr->GetActorLocation();
            FVector PlayerLocation = GetActorLocation();
            if(FVector::Dist(PlayerLocation, EmyLocation) <= 100.0f)
            {
                ActorItr->TakeDamage(MeleeDamage, FDamageEvent(), GetInstigatorController(), this);
            }
        }
    }
    
    for (TActorIterator<ARotateEnemy> ActorItr(GetWorld()); ActorItr; ++ActorItr)
    {
        if(ActorItr->IsA(ARotateEnemy::StaticClass()))
        {
            FVector EmyLocation = ActorItr->GetActorLocation();
            FVector PlayerLocation = GetActorLocation();
            if(FVector::Dist(PlayerLocation, EmyLocation) <= 100.0f)
            {
                ActorItr->TakeDamage(MeleeDamage, FDamageEvent(), GetInstigatorController(), this);
            }
        }
    }
    
}
void AEscapePlanCharacter::AddWeapon(){
    if (WeaponClass)
    {
        UWorld* World = GetWorld();
        if (World)
        {
            FActorSpawnParameters SpawnParams;
            SpawnParams.Owner = this;
            SpawnParams.Instigator = Instigator;
            // Need to set rotation like this because otherwise gun points down
            FRotator Rotation(90.0f, 90.0f, -90.0f);
            // Spawn the Weapon
            MyWeapon = World->SpawnActor<ARifleWeapon>(WeaponClass, FVector::ZeroVector,                                    Rotation, SpawnParams);
            
            if (MyWeapon){
                // This is attached to "WeaponPoint" which is defined in the skeleton
                MyWeapon->AttachToComponent(GetMesh(),FAttachmentTransformRules::KeepRelativeTransform,TEXT("RightHand"));
                MyWeapon->SetMyOwner(this);
            }
        }
    }
}

void AEscapePlanCharacter::Iteract()
{
    for(FConstPawnIterator Iterator = GetWorld()->GetPawnIterator(); Iterator; ++Iterator)
    {
        APawn* Pawn = *Iterator;
        FVector ItemLocation = Pawn->GetActorLocation();
        FVector PlayerLocation = GetActorLocation();
        if(Pawn->IsA(ADoorKey::StaticClass()))
        {
            if(FVector::Dist(PlayerLocation, ItemLocation) <= 120.0f)
            {
                Backpack.Add(Pawn);
                Pawn->Destroy();
            }
        }
        else if(Pawn->IsA(ADoor::StaticClass()))
        {
            if(FVector::Dist(PlayerLocation, ItemLocation) <= 120.0f)
            {
                for(int i = 0; i < Backpack.Num(); i++)
                {
                    if(Backpack[i]->IsA(ADoorKey::StaticClass()))
                    {
                        if(Cast<ADoorKey>(Backpack[i])->GetDoor() == Pawn)
                        {
                            if(Cast<ADoorKey>(Backpack[i])->GetDoor()->isExit()){
                                if(Cast<ADoorKey>(Backpack[i])->GetDoor()->isMap2Exit()){
                                    win = true;
									UGameplayStatics::OpenLevel(GetWorld(), FName(TEXT("end_menu")));
                                }
                                else{
                                    UGameplayStatics::OpenLevel(GetWorld(), FName(TEXT("map2")));
                                    //APlayerController* PC = Cast<APlayerController>(GetController());
                                    
                                }
                                
                            }
                            Cast<ADoorKey>(Backpack[i])->OpentheDoor();
                            Backpack.RemoveAt(i);
                            return;
                        }
                    }
                }
                //GEngine->AddOnScreenDebugMessage(1, 1.f, FColor::Green, FString::Printf(TEXT("This door requires a key")));
            }
        }
        else if(Pawn->IsA(AHealingPack::StaticClass()))
        {
            if(FVector::Dist(PlayerLocation, ItemLocation) <= 120.0f)
            {
                Backpack.Add(Pawn);
                Pawn->Destroy();
            }
        }
        else if(Pawn->IsA(ARifleWeapon::StaticClass())){
            if(FVector::Dist(PlayerLocation, ItemLocation) <= 200.0f)
            {
                //pickup weapon
                Pawn->Destroy();
                MeleeDamage = 20.0f;
                AddWeapon();
            }
        }
    }
}

bool AEscapePlanCharacter::IsInRange()
{
    bool IsInRange = false;
    for(FConstPawnIterator Iterator = GetWorld()->GetPawnIterator(); Iterator; ++Iterator)
    {
        APawn* Pawn = *Iterator;
        FVector ItemLocation = Pawn->GetActorLocation();
        FVector PlayerLocation = GetActorLocation();
        float Dist = FVector::Dist(PlayerLocation, ItemLocation);
        if(Pawn->IsA(ADoorKey::StaticClass()))
        {
            if(Dist <= Cast<ADoorKey>(Pawn)->GetRange())
            {
                IsInRange =  true;
                break;
            }
        }
//        else if(Pawn->IsA(ADoor::StaticClass()))
//        {
//            if(Dist <= Cast<ADoor>(Pawn)->GetRange())
//            {
//                IsInRange =  true;
//                break;
//            }
//        }
        else if(Pawn->IsA(AHealingPack::StaticClass()))
        {
            if(Dist <= Cast<AHealingPack>(Pawn)->GetRange())
            {
                IsInRange =  true;
                break;
            }
        }
        else if(Pawn->IsA(ARifleWeapon::StaticClass()))
        {
            if(Dist <= Cast<ARifleWeapon>(Pawn)->GetRange()&& MeleeDamage!=20.0f)
            {
                
                IsInRange =  true;
                break;
            }
        }
    }
    return IsInRange;
}
void AEscapePlanCharacter::Heal(){

    
    for(int i = 0; i < Backpack.Num(); i++)
    {
        if(Backpack[i]->IsA(AHealingPack::StaticClass())){
            Health+=20;
            Backpack.RemoveAt(i);
            return;
        }
    }
}
void AEscapePlanCharacter::GetHealed()
{
    Health -= 10.0f;
    if(Health > 100.0f)
    {
        Health = 100.0f;
    }
}

float AEscapePlanCharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent,
                                 AController* EventInstigator, AActor* DamageCauser)
{
    float ActualDamage = Super::TakeDamage(Damage, DamageEvent,
                                           EventInstigator, DamageCauser);
    if (ActualDamage > 0.0f)
    {
        printf("Take Damage");
        Health -= ActualDamage;
        if (Health <= 0.0f)
        {
            // We're dead, don't allow further damage
            bCanBeDamaged = false;
            float Duration = 0.1f;
            GetWorldTimerManager().SetTimer(DeathTimer, this, &AEscapePlanCharacter::PlayerDeath, Duration, false);
            APlayerController* PC = Cast<APlayerController>(GetController());
            if(PC)
            {
                PC->SetCinematicMode(true, true, true);
            }
        }
    }
    return ActualDamage;
}

void AEscapePlanCharacter::PlayerDeath()
{
    SetIsDead(true);
    GetMesh()->Deactivate();
	UGameplayStatics::OpenLevel(GetWorld(), FName(TEXT("end_menu")));

}

