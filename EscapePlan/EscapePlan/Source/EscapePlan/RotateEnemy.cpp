// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapePlan.h"
#include "RotateEnemy.h"
#include "EscapePlanCharacter.h"
#include "RotateEnemyAI.h"

// Sets default values
ARotateEnemy::ARotateEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARotateEnemy::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void ARotateEnemy::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void ARotateEnemy::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}


float ARotateEnemy::TakeDamage(float Damage, FDamageEvent const& DamageEvent,
                                AController* EventInstigator, AActor* DamageCauser)
{
    float ActualDamage = Super::TakeDamage(Damage, DamageEvent,
                                           EventInstigator, DamageCauser);
    if (ActualDamage > 0.0f)
    {
        Health -= ActualDamage;
        FVector Forward = GetActorForwardVector();
        FVector EndPos = GetActorLocation() - Forward * 100.0f;
        //  Cast<AAIController>(GetController())->MoveToLocation(EndPos);
        SetActorLocation(EndPos);
        
        if (Health <= 0.0f)
        {
            // We're dead, don't allow further damage
            bCanBeDamaged = false;
            float Duration = 0.1f;
            GetWorldTimerManager().SetTimer(DeathTimer, this, &ARotateEnemy::EnemyDeath, Duration, false);
        }
    }
    return ActualDamage;
}

void ARotateEnemy::EnemyDeath()
{
	Destroy();
	//SetIsDead(true);
}

void ARotateEnemy::StartAttack()
{
    float Duration = PlayAnimMontage(AttackAnim);
    GetWorldTimerManager().SetTimer(AttackTimer, this, &ARotateEnemy::CauseDamage, Duration, true);
}

void ARotateEnemy::StopAttack()
{
	//if (this != nullptr)
	{
		StopAnimMontage(AttackAnim);
		GetWorldTimerManager().ClearTimer(AttackTimer);
	}
}

void ARotateEnemy::CauseDamage()
{
	APawn * playerPawn = UGameplayStatics::GetPlayerPawn(this, 0);
	if (playerPawn != nullptr)
	{
		playerPawn->TakeDamage(Damage, FDamageEvent(), GetInstigatorController(), this);
	}
}
