// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "InventoryWidget.h"
#include "GameFramework/GameMode.h"
#include "EscapePlanGameMode.generated.h"

UCLASS(minimalapi)
class AEscapePlanGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AEscapePlanGameMode();
    int ItemNum = 1;
    
    UPROPERTY(EditDefaultsOnly)
    TSubclassOf<UInventoryWidget> InventoryWidgetBP;
};



