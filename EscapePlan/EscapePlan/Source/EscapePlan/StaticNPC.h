// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "StaticNPC.generated.h"

UCLASS()
class ESCAPEPLAN_API AStaticNPC : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AStaticNPC();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	bool IsInRange();
protected:
    UPROPERTY(EditAnywhere, Category = Weapon)
    TSubclassOf<class ARifleWeapon> WeaponClass;
private:
    class ARifleWeapon* MyWeapon;
	
};
