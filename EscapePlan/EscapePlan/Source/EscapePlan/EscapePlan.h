// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#ifndef __ESCAPEPLAN_H__
#define __ESCAPEPLAN_H__

#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/UMGStyle.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/Slate/SObjectWidget.h"
#include "Runtime/UMG/Public/IUMGModule.h"
#include "EngineMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogEscapePlan, Log, All);


#endif
