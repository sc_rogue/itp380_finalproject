// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapePlan.h"
#include "RotateEnemyAI.h"
#include "EscapePlanCharacter.h"
#include <iostream>
#include "RotateEnemy.h"
using namespace std;



void ARotateEnemyAI::BeginPlay() {
	Super::BeginPlay();
	CurrentState = START;
	CheckRange = 300.0f;
	AttackRange = 150.0f;
	RotateLocation = GetPawn()->GetActorLocation();
	//Rotate();
}

void ARotateEnemyAI::Tick(float DeltaSeconds)
{
	if (CurrentState == START || CurrentState == ROTATE) {
		Rotate();
		CurrentState = WAIT;
	}
	if (CurrentState == CHASE) {
		APawn* pawn = UGameplayStatics::GetPlayerPawn(this, 0);
		MoveToActor(pawn);
	}
	if (CurrentState == ATTACK) {
		APawn* pawn = UGameplayStatics::GetPlayerPawn(this, 0);
		if (!pawn)
		{
			return;
		}
		Distance = FVector::Dist(pawn->GetActorLocation(), GetPawn()->GetActorLocation());
		if (Distance < AttackRange) {
			//AttackPlayer();
		}
		if (Distance > AttackRange && Distance < CheckRange) {
            MoveToActor(pawn);
            CurrentState = CHASE;
            Cast<ARotateEnemy>(GetPawn())->StopAttack();
		}
		if (Distance > CheckRange) {

			FVector CurrentLocation = GetPawn()->GetActorLocation();
			MoveToLocation(RotateLocation);
			CurrentState = BACK;
            Cast<ARotateEnemy>(GetPawn())->StopAttack();

		}
	}

}

void ARotateEnemyAI::Rotate()
{
	//UE_LOG(LogClass, Log, TEXT("This is a testing statement.%s"));
	FRotator ActorRotation = GetPawn()->GetActorRotation();
	ActorRotation.Yaw += 30;
	GetPawn()->SetActorRotation(ActorRotation);
	if (Trace())
	{
		GetWorldTimerManager().ClearTimer(RotateTimer);
		return;
	}
	GetWorldTimerManager().SetTimer(RotateTimer, this, &ARotateEnemyAI::Rotate, 1.0f, false);
}

bool ARotateEnemyAI::Trace()
{
	static FName SightTraceTag = FName(TEXT("SightTrace"));
	FVector Forward = GetPawn()->GetActorForwardVector();
	// Calculate end position 
	FVector EndPos = RotateLocation + CheckRange * Forward;
	// Perform trace to retrieve hit info 
	FCollisionQueryParams TraceParams(SightTraceTag, true, Instigator);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;
	// This fires the ray and checks against all objects w/ collision 
	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingleByObjectType(Hit, RotateLocation, EndPos,
		FCollisionObjectQueryParams::AllObjects, TraceParams);
	if (Hit.bBlockingHit)
	{
		AEscapePlanCharacter* Player = Cast<AEscapePlanCharacter>(Hit.GetActor());
		if (Player)
		{
			CurrentState = CHASE;
			return true;
		}
	}
	return false;
}

bool ARotateEnemyAI::Check()
{
	return false;


}

void ARotateEnemyAI::AttackPlayer()
{
	ARotateEnemy* Rotate = Cast<ARotateEnemy>(GetPawn());
	Rotate->StartAttack();
}

void ARotateEnemyAI::ChasePlayer()
{
	APawn* pawn = UGameplayStatics::GetPlayerPawn(this, 0);
	MoveToActor(pawn);
}


void ARotateEnemyAI::OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result) {
	if (Result == EPathFollowingResult::Success) {
		if (CurrentState == CHASE)
        {
			CurrentState = ATTACK;
			AttackPlayer();
		}
		if (CurrentState == BACK)
        {
			CurrentState = ROTATE;
		}
	}
}
