// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "EscapePlanCharacter.h"
#include "StaticNPC.h"

#include "RifleWeapon.generated.h"

UCLASS()
class ESCAPEPLAN_API ARifleWeapon : public APawn
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARifleWeapon();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
    float GetRange() { return Range; }
    void SetMyOwner(AEscapePlanCharacter* owner){MyOwner = owner;}
    AEscapePlanCharacter* GetMyOwner(){return MyOwner;}

	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
protected:
    UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Weapon)
    USkeletalMeshComponent* WeaponMesh;
    float Range = 200.0f;
    AEscapePlanCharacter* MyOwner;

	
};
