// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "WalkingEnemy.generated.h"

UCLASS()
class ESCAPEPLAN_API AWalkingEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AWalkingEnemy();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

    virtual float TakeDamage(float Damage, FDamageEvent const& DamageEvent,
                            AController* EventInstigator, AActor* DamageCauser) override;
	
    void EnemyDeath();
    
    UPROPERTY(EditAnywhere)
    TArray<class ATargetPoint*> SpotPoints;
    
    void StartAttack();
    void StopAttack();
    void CauseDamage();

protected:
    UPROPERTY(EditAnywhere, Category = Damage)
    float Health = 40.f;
    
    UPROPERTY(EditDefaultsOnly)
    class UAnimMontage* DeathAnim;
    
    UPROPERTY(EditDefaultsOnly)
    class UAnimMontage* AttackAnim;
    
    FTimerHandle AttackTimer;
    
    UPROPERTY(EditAnywhere, Category = Damage)
    float Damage = 10.0f;

    

    
private:
    FTimerHandle DeathTimer;

};
