// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "PlayerHUD.generated.h"

/**
 * 
 */
UCLASS()
class ESCAPEPLAN_API APlayerHUD : public AHUD
{
	GENERATED_BODY()
	
public:
    
    APlayerHUD();
    
    UPROPERTY()
    UFont* HUDFont;
	
    virtual void DrawHUD() override;

private:
    int Yposition = 50;
};
