// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "InventoryWidget.h"
#include "GameFramework/PlayerController.h"
#include "EscapePlanPlayerController.generated.h"

UCLASS()
class AEscapePlanPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AEscapePlanPlayerController();
    virtual void Possess(APawn* InPawn) override;
protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Navigate player to the current mouse cursor location. */
	void MoveToMouseCursor();

	/** Navigate player to the current touch location. */
	void MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location);
	
	/** Navigate player to the given world location. */
	void SetNewMoveDestination(const FVector DestLocation);

	/** Input handlers for SetDestination action. */
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();
    
    void MoveForward(float Value);
    void MoveRight(float Value);
    
    void OnJump();
    void OnStartMeleeAttack();
    void OnStopMeleeAttack();
    void OnIteraction();
    void ShowInventory();
    void HideInventory();
    
    //heal
    void Heal();
    
    //UPROPERTY(EditDefaultsOnly)
    TSubclassOf<UInventoryWidget> InventoryWidgetBP;
    
private:
    UInventoryWidget* InventoryWidgetRef;
    bool bIsInventoryOpen = false;
};


