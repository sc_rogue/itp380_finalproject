// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "EscapePlanCharacter.generated.h"

UCLASS(Blueprintable)
class AEscapePlanCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AEscapePlanCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

    virtual float TakeDamage(float Damage, FDamageEvent const& DamageEvent,
                             AController* EventInstigator, AActor* DamageCauser) override;
    
    void StartJump();
    void StartMeleeAttack();
    void StopMeleeAttack();
    void Attack();
    void Iteract();
    void GetHealed();
    float GetHealth() { return Health; }
    bool IsInRange();
    TArray<APawn*> GetBackpack() { return Backpack; }
    bool GetIsDead() { return IsDead; }
    void SetIsDead(bool Dead) { IsDead = true; }
    bool GetIsWon() { return win; }
    void Heal();
protected:
    void PlayerDeath();
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category =Mesh)
    UStaticMeshComponent * StaticMesh;
    UPROPERTY(EditAnywhere, Category = Weapon)
    TSubclassOf<class ARifleWeapon> WeaponClass;
private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;
    
    UPROPERTY(EditAnywhere, Category = Damage)
    float MeleeDamage = 10.0f;
    
    UPROPERTY(EditAnywhere, Category = Damage)
    float Health = 100.0f;
    UPROPERTY(EditAnywhere, Category = Mesh)
    float test = 100.0f;
    UPROPERTY(EditAnywhere, Category = Mesh)
    UStaticMeshComponent * test2;
    
    UPROPERTY(EditDefaultsOnly)
    class UAnimMontage* JumpAnim;
    
    UPROPERTY(EditDefaultsOnly)
    class UAnimMontage* RunningJumpAnim;
    
    UPROPERTY(EditDefaultsOnly)
    class UAnimBlueprint* PlayerAnimBP;

    UPROPERTY(EditDefaultsOnly)
    class UAnimMontage* AttackAnim;
    
    void AddWeapon();
    
    FTimerHandle AttackTimer;
    FTimerHandle JumpTimer;
    FTimerHandle DeathTimer;
    TArray<APawn*> Backpack;
    bool win = false;
    bool IsDead = false;
    bool IsRunning;
    class ARifleWeapon* MyWeapon;
};

