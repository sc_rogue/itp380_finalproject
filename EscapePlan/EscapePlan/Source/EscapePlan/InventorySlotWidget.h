// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "InventorySlotWidget.generated.h"

/**
 * 
 */
UCLASS()
class ESCAPEPLAN_API UInventorySlotWidget : public UUserWidget
{
	GENERATED_BODY()
public:
    UFUNCTION(BlueprintCallable, Category = UI)
    void SetItemTexture(APawn* Item);
	
protected:
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
    UTexture2D* ItemTexture;
	
};
