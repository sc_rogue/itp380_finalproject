// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "RotateEnemy.generated.h"

UCLASS()
class ESCAPEPLAN_API ARotateEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARotateEnemy();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

    virtual float TakeDamage(float Damage, FDamageEvent const& DamageEvent,
                             AController* EventInstigator, AActor* DamageCauser) override;
    
	void EnemyDeath();
	void StartAttack();
    void StopAttack();
	void CauseDamage();
private:
	UPROPERTY(EditAnywhere, Category = Damage)
    float Health = 40.f;

	UPROPERTY(EditDefaultsOnly)
    class UAnimMontage* DeathAnim;
	
    FTimerHandle DeathTimer;
    
    UPROPERTY(EditDefaultsOnly)
    class UAnimMontage* AttackAnim;
    
    FTimerHandle AttackTimer;

	UPROPERTY(EditAnywhere, Category = Damage)
    float Damage = 10.0f;

};
