// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "EscapePlan.h"
#include "EscapePlanPlayerController.h"
#include "EscapePlanGameMode.h"
#include "AI/Navigation/NavigationSystem.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "EscapePlanCharacter.h"

AEscapePlanPlayerController::AEscapePlanPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void AEscapePlanPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	// keep updating the destination every tick while desired
	if (bMoveToMouseCursor)
	{
		MoveToMouseCursor();
	}
}

void AEscapePlanPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

//	InputComponent->BindAction("SetDestination", IE_Pressed, this, &AEscapePlanPlayerController::OnSetDestinationPressed);
//	InputComponent->BindAction("SetDestination", IE_Released, this, &AEscapePlanPlayerController::OnSetDestinationReleased);
//
//	// support touch devices 
//	InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AEscapePlanPlayerController::MoveToTouchLocation);
//	InputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AEscapePlanPlayerController::MoveToTouchLocation);
//
//	InputComponent->BindAction("ResetVR", IE_Pressed, this, &AEscapePlanPlayerController::OnResetVR);
    
    InputComponent->BindAxis("MoveForward", this, &AEscapePlanPlayerController::MoveForward);
    InputComponent->BindAxis("MoveRight", this, &AEscapePlanPlayerController::MoveRight);
    InputComponent->BindAction("Jump", IE_Pressed, this, &AEscapePlanPlayerController::OnJump);
    InputComponent->BindAction("Attack", IE_Pressed, this, &AEscapePlanPlayerController::OnStartMeleeAttack);
    InputComponent->BindAction("PickUp", IE_Pressed, this, &AEscapePlanPlayerController::OnIteraction);
    InputComponent->BindAction("Attack", IE_Released, this, &AEscapePlanPlayerController::OnStopMeleeAttack);
    InputComponent->BindAction("Heal", IE_Pressed, this, &AEscapePlanPlayerController::Heal);
    InputComponent->BindAction("ShowInventory", IE_Pressed, this, &AEscapePlanPlayerController::ShowInventory);
}

void AEscapePlanPlayerController::Possess(APawn* InPawn)
{
    Super::Possess(InPawn);
    
    AGameMode *Mode = UGameplayStatics::GetGameMode(GetWorld());
    if(Mode)
    {
        InventoryWidgetBP = Cast<AEscapePlanGameMode>(Mode)->InventoryWidgetBP;
    
        if (InventoryWidgetBP)
        {
            //Create the Inventory Widget based on the Blueprint reference we will input from within the Editor
            InventoryWidgetRef = CreateWidget<UInventoryWidget>(this, InventoryWidgetBP);
        }
    }
    
    ////Initial value
   // bIsInventoryOpen = false;
}

void AEscapePlanPlayerController::ShowInventory()
{
    ACharacter* Char = GetCharacter();
    if(Char)
    {
        if(InventoryWidgetRef)
        {
            if (bIsInventoryOpen)
            {
                //Mark the inventory as closed
                bIsInventoryOpen = false;
                
                //Remove it from the viewport
                InventoryWidgetRef->RemoveFromViewport();
                    InventoryWidgetRef = CreateWidget<UInventoryWidget>(this, InventoryWidgetBP);
            }
            else
            {
                bIsInventoryOpen = true;
                InventoryWidgetRef->ItemsArray = Cast<AEscapePlanCharacter>(Char)->GetBackpack();
                InventoryWidgetRef->Show();
            }
        }
    }
}

void AEscapePlanPlayerController::HideInventory()
{
    if(InventoryWidgetRef)
    {
        InventoryWidgetRef->ItemsArray.Empty();
        InventoryWidgetRef->RemoveFromViewport();
    }
}

void AEscapePlanPlayerController::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AEscapePlanPlayerController::MoveToMouseCursor()
{
	if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	{
		if (AEscapePlanCharacter* MyPawn = Cast<AEscapePlanCharacter>(GetPawn()))
		{
			if (MyPawn->GetCursorToWorld())
			{
				UNavigationSystem::SimpleMoveToLocation(this, MyPawn->GetCursorToWorld()->GetComponentLocation());
			}
		}
	}
	else
	{
		// Trace to see what is under the mouse cursor
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);

		if (Hit.bBlockingHit)
		{
			// We hit something, move there
			SetNewMoveDestination(Hit.ImpactPoint);
		}
	}
}

void AEscapePlanPlayerController::SetNewMoveDestination(const FVector DestLocation)
{
	APawn* const MyPawn = GetPawn();
	if (MyPawn)
	{
		UNavigationSystem* const NavSys = GetWorld()->GetNavigationSystem();
		float const Distance = FVector::Dist(DestLocation, MyPawn->GetActorLocation());

		// We need to issue move command only if far enough in order for walk animation to play correctly
		if (NavSys && (Distance > 120.0f))
		{
			NavSys->SimpleMoveToLocation(this, DestLocation);
		}
	}
}

void AEscapePlanPlayerController::OnSetDestinationPressed()
{
	// set flag to keep updating destination until released
	bMoveToMouseCursor = true;
}

void AEscapePlanPlayerController::OnSetDestinationReleased()
{
	// clear flag to indicate we should stop updating the destination
	bMoveToMouseCursor = false;
}

void AEscapePlanPlayerController::MoveForward(float Value)
{
    if(Value != 0.0f)
    {
        APawn* pawn = GetPawn();
        if(pawn != nullptr)
        {
            pawn->AddMovementInput(FVector(1.0f, 0.0f, 0.0f), Value);
        }
    }
}

void AEscapePlanPlayerController::MoveRight(float Value)
{
    if(Value != 0.0f)
    {
        APawn* pawn = GetPawn();
        if(pawn != nullptr)
        {
            pawn->AddMovementInput(FVector(0.0f, 1.0f, 0.0f), Value);
        }
    }
}

void AEscapePlanPlayerController::OnJump()
{
    ACharacter* c = GetCharacter();
    if(c != nullptr && !Cast<AEscapePlanCharacter>(c)->GetIsDead())
    {
        Cast<AEscapePlanCharacter>(c)->StartJump();
    }
}

void AEscapePlanPlayerController::OnStartMeleeAttack()
{
    ACharacter* c = GetCharacter();
    if(c != nullptr && !Cast<AEscapePlanCharacter>(c)->GetIsDead())
    {
        Cast<AEscapePlanCharacter>(c)->StartMeleeAttack();
    }
}

void AEscapePlanPlayerController::OnStopMeleeAttack()
{
    ACharacter* c = GetCharacter();
    if(c != nullptr && !Cast<AEscapePlanCharacter>(c)->GetIsDead())
    {
        Cast<AEscapePlanCharacter>(c)->StopMeleeAttack();
    }
}

void AEscapePlanPlayerController::OnIteraction()
{
    ACharacter* c = GetCharacter();
    if(c != nullptr && !Cast<AEscapePlanCharacter>(c)->GetIsDead())
    {
        Cast<AEscapePlanCharacter>(c)->Iteract();
    }
}

void AEscapePlanPlayerController::Heal()
{
    ACharacter* c = GetCharacter();
    if(c != nullptr && !Cast<AEscapePlanCharacter>(c)->GetIsDead())
    {
        Cast<AEscapePlanCharacter>(c)->Heal();
    }
}

