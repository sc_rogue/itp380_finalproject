// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapePlan.h"
#include "HealingPack.h"
#include "EscapePlanGameMode.h"

// Sets default values
AHealingPack::AHealingPack()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AHealingPack::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AHealingPack::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void AHealingPack::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

