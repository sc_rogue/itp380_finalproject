// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "RotateEnemyAI.generated.h"

/**
 * 
 */
UCLASS()
class ESCAPEPLAN_API ARotateEnemyAI : public AAIController
{
	GENERATED_BODY()
public:
	enum States {
		START,
		CHASE,
		ATTACK,
		DEAD,
		ROTATE,
		WAIT,
		BACK
	};
	void BeginPlay() override;


	void Tick(float DeltaSeconds) override;

	void Rotate();

	bool Trace();

	bool Check();

	void AttackPlayer();

	void ChasePlayer();

	void OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result);


private:
	States CurrentState;
	float AttackRange;
	float CheckRange;
	FVector RotateLocation;
	float Distance;
	FTimerHandle RotateTimer;
	
	
	
	
};
