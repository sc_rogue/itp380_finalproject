// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "Door.generated.h"

UCLASS()
class ESCAPEPLAN_API ADoor : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ADoor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

    float GetRange() { return Range; }
    bool isExit() {return exit;}
    bool isMap2Exit() {return map2exit;}

    
private:
    float Range = 120.0f;
    UPROPERTY(EditAnywhere)
    bool exit = false;
    UPROPERTY(EditAnywhere)
    bool map2exit = false;

    
	
};
