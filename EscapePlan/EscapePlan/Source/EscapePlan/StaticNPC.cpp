// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapePlan.h"
#include "RifleWeapon.h"
#include "StaticNPC.h"
#include "Engine.h"


// Sets default values
AStaticNPC::AStaticNPC()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AStaticNPC::BeginPlay()
{
	Super::BeginPlay();
    
}

// Called every frame
void AStaticNPC::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

// Called to bind functionality to input
void AStaticNPC::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

bool AStaticNPC::IsInRange()
{
	APawn* pawn = UGameplayStatics::GetPlayerPawn(this, 0);
	FVector position = GetActorLocation();
	if (FVector::Dist(position, pawn->GetActorLocation()) <= 100)
	{
		return true;
	}
	return false;
}

