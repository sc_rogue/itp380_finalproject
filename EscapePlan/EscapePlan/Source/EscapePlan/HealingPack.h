// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "HealingPack.generated.h"

UCLASS()
class ESCAPEPLAN_API AHealingPack : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AHealingPack();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

    float GetRange() { return Range; }
    
    FString GetName() { return Name; }
    
    FORCEINLINE UTexture2D* GetItemTexture() { return ItemTexture; }
    
private:
    float Range = 260.0f;
	FString Name = "HealingPack";
    
    UPROPERTY(EditAnywhere, Category = "ItemProperties")
    UTexture2D* ItemTexture;
};
