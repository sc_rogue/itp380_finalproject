// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapePlan.h"
#include "DoorKey.h"
#include "HealingPack.h"
#include "InventorySlotWidget.h"

void UInventorySlotWidget::SetItemTexture(APawn* Item)
{
    //If the item is valid update the widget's texture.
    //If not, assign a null ptr to it so the widget won't broadcast wrong information to the player
    
    if(Item)
    {
        if(Item->IsA(ADoorKey::StaticClass()))
        {
            ItemTexture = Cast<ADoorKey>(Item)->GetItemTexture();
        }
        else if(Item->IsA(AHealingPack::StaticClass()))
        {
            ItemTexture = Cast<AHealingPack>(Item)->GetItemTexture();
        }
    }
    else
    {
        ItemTexture = nullptr;
    }
}


