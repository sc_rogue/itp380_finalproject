// Fill out your copyright notice in the Description page of Project Settings.

#include "EscapePlan.h"
#include "DoorKey.h"
#include "EscapePlanGameMode.h"

// Sets default values
ADoorKey::ADoorKey()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    //ItemTexture = CreateDefaultSubobject<UTexture2D>(FName("key"));
}

// Called when the game starts or when spawned
void ADoorKey::BeginPlay()
{
	Super::BeginPlay();
    AGameMode* Mode = UGameplayStatics::GetGameMode(GetWorld());
    int Num = Cast<AEscapePlanGameMode>(Mode)->ItemNum;
    FString Convert = FString::FromInt(Num);
    FString Door = "Doorkey_";
    Door += Convert;
    Name = Door;
    Cast<AEscapePlanGameMode>(Mode)->ItemNum++;
	
}
void ADoorKey::OpentheDoor(){
    if(thedoor){
        thedoor->Destroy();
    }
}

// Called every frame
void ADoorKey::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void ADoorKey::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

